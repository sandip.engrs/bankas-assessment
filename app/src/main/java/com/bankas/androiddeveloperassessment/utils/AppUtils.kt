package com.bankas.androiddeveloperassessment.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.marginLeft
import java.lang.Exception
import java.util.regex.Pattern
import kotlin.reflect.KClass

fun <T : Activity> AppCompatActivity.openActivity(
    activity: KClass<T>,
    extras: Bundle? = null,
    flags: String? = null
) {
    val intent = Intent(this, activity.java)
    flags?.let {
        if (it == "clearStack") {

            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
        }
    }
    extras?.let {
        intent.putExtra("extras", it)
    }
    startActivity(intent)
}

fun View.setMargin(marginTLeft: Int, marginTop: Int, marginRight: Int, marginBottom: Int) {
    val menuLayoutParams = this.layoutParams as ViewGroup.MarginLayoutParams
    menuLayoutParams.setMargins(marginLeft, marginTop, marginRight, marginBottom)
    this.layoutParams = menuLayoutParams
}

fun rejexValidator(rejex: String, value: String): Boolean {

    if(rejex.isNullOrEmpty()) return true

    try{

        var pattern = Pattern.compile(rejex)
        var matcher = pattern.matcher(value)

        return matcher.matches()

    }catch (e: Exception){

        return true
    }

}

fun Activity.checkInternetConnection(): Boolean {

    val connMgr: ConnectivityManager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    val activeNetworkInfo = connMgr.activeNetworkInfo
    if (activeNetworkInfo != null) {

        return if (activeNetworkInfo.type == ConnectivityManager.TYPE_WIFI) {

            true

        } else activeNetworkInfo.type == ConnectivityManager.TYPE_MOBILE
    }

    return false

}