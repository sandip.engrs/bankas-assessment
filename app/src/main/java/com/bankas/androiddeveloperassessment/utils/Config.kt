package com.bankas.androiddeveloperassessment.utils

val ANIM_SMALL: Long = 200
val ANIM_MEDIUM: Long = 400
val ANIM_LARGE: Long = 600