package com.bankas.androiddeveloperassessment.utils

import android.content.Context
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.bankas.androiddeveloperassessment.R
import kotlinx.android.synthetic.main.field_inputtype.view.*
import kotlinx.android.synthetic.main.field_inputtype.view.tv_error
import kotlinx.android.synthetic.main.field_inputtype.view.tv_field_name
import kotlinx.android.synthetic.main.field_inputtype.view.tv_required
import kotlinx.android.synthetic.main.field_spinner.view.*

class CustomFieldView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr), TextWatcher {

    init {
        LayoutInflater.from(context).inflate(R.layout.field_inputtype, this, true)
    }

    fun updateValue(name: String, fieldType: String, placeholder: String, hint: String, isRequired: String){

        tv_field_name.text = placeholder

        if(isRequired == "true"){
            tv_required.visibility = View.VISIBLE
        }else{
            tv_required.visibility = View.GONE
        }
        ed_field.hint = hint
        ed_field.addTextChangedListener(this)
        if(fieldType == "string"){

            ed_field.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_NORMAL

        }else if(fieldType == "int"){

            ed_field.inputType =  InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_SIGNED

        }
    }

    fun showError(message: String){

        tv_error.text = message
        tv_error.visibility = View.VISIBLE

    }

    fun hideError(){

        tv_error.visibility = View.GONE
    }

    fun getValue(): String {

        return ed_field.text.toString().trim()
    }

    fun validation(isRequired: String, rejex: String): Boolean{

        var value = getValue()

        if(isRequired == "true"){

            if(value.isNullOrEmpty()) {

                showError("Field is required")
                return false

            }else{

                if(rejexValidator(rejex,value)){
                    return true
                }else{
                    showError("Input is not valid")
                    return false
                }

            }

        }else{

            if(rejexValidator(rejex,value)){
                return true
            }else{
                showError("Input is not valid")
                return false
            }
        }

    }

    override fun afterTextChanged(s: Editable?) {

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        hideError()
    }

}