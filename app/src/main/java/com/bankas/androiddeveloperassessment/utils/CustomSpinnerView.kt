package com.bankas.androiddeveloperassessment.utils

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.FrameLayout
import com.bankas.androiddeveloperassessment.R
import com.bankas.androiddeveloperassessment.model.Value
import kotlinx.android.synthetic.main.field_spinner.view.*

class CustomSpinnerView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr), AdapterView.OnItemSelectedListener {

    init {
        LayoutInflater.from(context).inflate(R.layout.field_spinner, this, true)
    }

    fun updateValue(name: String, fieldType: String,placeholder: String, hint: String, isRequired: String, values: List<Value>){

        tv_field_name.text = placeholder

        if(isRequired == "true"){
            tv_required.visibility = View.VISIBLE
        }else{
            tv_required.visibility = View.GONE
        }
        sp_dropdown.prompt = hint
        sp_dropdown.onItemSelectedListener = this

        var newValueList = mutableListOf<Value>()
        newValueList.add(Value("-1", hint))
        newValueList.addAll(values)

        val dropDownAdapter: ArrayAdapter<Value> = ArrayAdapter<Value>(context,
            R.layout.spinner_tv, R.id.tv_title, newValueList)
        sp_dropdown.adapter = dropDownAdapter

        sp_dropdown.adapter = dropDownAdapter

    }

    fun showError(message: String){

        tv_error.text = message
        tv_error.visibility = View.VISIBLE

    }

    fun hideError(){
        tv_error.visibility = View.GONE
    }

    fun getValue(): Value {
        return sp_dropdown.selectedItem as Value
    }

    fun validation(isRequired: String): Boolean{

        var value = getValue()

        if(isRequired == "true"){

            if(value == null || value.id == "-1") {

                showError("Field is required")
                return false

            }else{

                return true

            }

        }else{

            return true
        }

    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

        hideError()
    }
}