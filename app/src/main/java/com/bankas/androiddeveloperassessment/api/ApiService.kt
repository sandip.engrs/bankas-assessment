package com.bankas.androiddeveloperassessment.api

import com.bankas.androiddeveloperassessment.model.ServiceResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService{

    @GET("task/{type}")
    fun getServiceType(@Path("type") id: Int): Call<ServiceResponse>

}