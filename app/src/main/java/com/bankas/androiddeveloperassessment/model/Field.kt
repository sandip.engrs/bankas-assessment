package com.bankas.androiddeveloperassessment.model

data class Field(
    val hint_text: String,
    val is_mandatory: String,
    val name: String,
    val placeholder: String,
    val regex: String,
    val type: Type,
    val ui_type: UiType
)