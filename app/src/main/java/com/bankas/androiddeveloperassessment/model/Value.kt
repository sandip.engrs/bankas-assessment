package com.bankas.androiddeveloperassessment.model

data class Value(
    val id: String,
    val name: String
){
    override fun toString(): String {
        return name
    }
}