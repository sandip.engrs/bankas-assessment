package com.bankas.androiddeveloperassessment.model

data class Result(
    val fields: List<Field>,
    val number_of_fields: Int,
    val operator_name: String,
    val screen_title: String,
    val service_id: String
)