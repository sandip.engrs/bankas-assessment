package com.bankas.androiddeveloperassessment.model


data class UiType(
    val type: String,
    val values: List<Value>
)