package com.bankas.androiddeveloperassessment.model

data class ServiceResponse(
    val message: String,
    val result: Result,
    val status: String
)