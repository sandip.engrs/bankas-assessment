package com.bankas.androiddeveloperassessment.model

data class Type(
    val array_name: String,
    val data_type: String,
    val is_array: String
)