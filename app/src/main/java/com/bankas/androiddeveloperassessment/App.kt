package com.bankas.androiddeveloperassessment

import android.app.Application
import android.app.Service
import android.util.Log
import com.bankas.androiddeveloperassessment.api.ApiService
import com.bankas.androiddeveloperassessment.viewmodel.ServiceViewModel
import com.google.gson.JsonParseException
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.SocketTimeoutException
import java.util.concurrent.TimeUnit

class App : Application(){

    private val appModule = module {

        single<ApiService> {

            get<Retrofit>().create(ApiService::class.java)
        }

        single {
            val okHttpClient = OkHttpClient().newBuilder().apply {

                writeTimeout(65, TimeUnit.SECONDS)
                callTimeout(65, TimeUnit.SECONDS)
                readTimeout(65, TimeUnit.SECONDS)
                connectTimeout(65, TimeUnit.SECONDS)

                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                addInterceptor(interceptor)

            }.retryOnConnectionFailure(true)
                .build()
            okHttpClient
        }

        single {

            Retrofit.Builder()
                .baseUrl("https://api-staging.bankaks.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(get())
                .build()

        }

        viewModel {
            ServiceViewModel(
                service = get()
            )
        }
    }

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(applicationContext)
            modules(listOf(appModule))
        }
    }

}
