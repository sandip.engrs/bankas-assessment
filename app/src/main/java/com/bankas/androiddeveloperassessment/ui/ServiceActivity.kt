package com.bankas.androiddeveloperassessment.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.bankas.androiddeveloperassessment.BaseActivity
import com.bankas.androiddeveloperassessment.R
import com.bankas.androiddeveloperassessment.api.ApiService
import com.bankas.androiddeveloperassessment.model.Result
import com.bankas.androiddeveloperassessment.model.ServiceResponse
import com.bankas.androiddeveloperassessment.model.Value
import com.bankas.androiddeveloperassessment.utils.CustomFieldView
import com.bankas.androiddeveloperassessment.utils.CustomSpinnerView
import com.bankas.androiddeveloperassessment.utils.checkInternetConnection
import com.bankas.androiddeveloperassessment.utils.setMargin
import com.bankas.androiddeveloperassessment.viewmodel.ServiceViewModel
import kotlinx.android.synthetic.main.activity_service.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ServiceActivity : BaseActivity() {

    private val service: ApiService by inject()

    var serviceResponse: ServiceResponse? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service)

        tv_toolbar_icon.setOnClickListener {
            finish()
        }

        var serviceType = intent.getBundleExtra("extras").getInt("type")
        getServiceDetail(serviceType)


        next_button.setOnClickListener {

            if(validation()){

                Toast.makeText(applicationContext,  "Successful",Toast.LENGTH_SHORT).show()
                //call api
            }else{

                //return error
            }

        }
    }

    fun validation(): Boolean {

        var isValidate = false

        run loop@{

            serviceResponse?.result?.fields?.forEach {

                if (it.ui_type.type == "textfield") {

                    var view = container_servcie_field.findViewWithTag<CustomFieldView>(it.name)

                    if (view.validation(it.is_mandatory, it.regex)) {

                        Log.i("Validaiton","true")
                        isValidate = true


                    } else {

                        Log.i("Validaiton","false")
                        isValidate = false
                        return@loop
                    }

                } else if (it.ui_type.type == "dropdown") {

                    var view = container_servcie_field.findViewWithTag<CustomSpinnerView>(it.name)

                    if (view.validation(it.is_mandatory)) {

                        isValidate = true


                    } else {

                        isValidate = false
                        return@loop
                    }

                }
            }
        }

        return isValidate
    }

    fun getServiceDetail(type: Int) {

        if(this.checkInternetConnection()){

            showProgressDialog()

            service.getServiceType(type).enqueue(object : Callback<ServiceResponse> {
                override fun onFailure(call: Call<ServiceResponse>, t: Throwable) {

                    hideProgressDialog()

                }

                override fun onResponse(
                    call: Call<ServiceResponse>,
                    response: Response<ServiceResponse>
                ) {

                    serviceResponse = response.body()
                    hideProgressDialog()
                    setupLayout(response.body()?.result)

                }

            })

        }else{
            Toast.makeText(applicationContext,  "Check your internet connection",Toast.LENGTH_LONG).show()
            finish()
        }


    }

    fun setupLayout(result: Result?) {

        tv_toolbar_title.text = result?.screen_title

        result?.fields?.forEach {

            if (it.ui_type.type == "textfield") {

                var textFieldView = CustomFieldView(context = this)
                textFieldView.updateValue(
                    it.name,
                    it.type.data_type,
                    it.placeholder,
                    it.hint_text,
                    it.is_mandatory
                )
                textFieldView.tag = it.name
                container_servcie_field.addView(textFieldView)
                textFieldView.setMargin(
                    0,
                    0,
                    0,
                    resources.getDimension(R.dimen.field_margin).toInt()
                )
                container_servcie_field.requestLayout()

            } else if (it.ui_type.type == "dropdown") {

                var textSpinnerView = CustomSpinnerView(context = this)
                textSpinnerView.updateValue(
                    it.name,
                    it.type.data_type,
                    it.placeholder,
                    it.hint_text,
                    it.is_mandatory,
                    it.ui_type.values
                )
                textSpinnerView.tag = it.name
                container_servcie_field.addView(textSpinnerView)
                textSpinnerView.setMargin(
                    0,
                    0,
                    0,
                    resources.getDimension(R.dimen.field_margin).toInt()
                )
                container_servcie_field.requestLayout()

            }

        }


    }

}