package com.bankas.androiddeveloperassessment.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.bankas.androiddeveloperassessment.R
import com.bankas.androiddeveloperassessment.utils.ANIM_LARGE
import com.bankas.androiddeveloperassessment.utils.openActivity

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({

            this.openActivity(activity = MainActivity::class, flags = "clearTask")
            finish()

        },ANIM_LARGE)
    }
}