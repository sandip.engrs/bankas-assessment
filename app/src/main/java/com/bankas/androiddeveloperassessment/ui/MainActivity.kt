package com.bankas.androiddeveloperassessment.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.bankas.androiddeveloperassessment.BaseActivity
import com.bankas.androiddeveloperassessment.R
import com.bankas.androiddeveloperassessment.utils.openActivity
import com.bankas.androiddeveloperassessment.viewmodel.ServiceViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity() {

    val serviceTypeList = listOf<String>("Select service type","Type 1","Type 2", "Type 3")


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupInitView()

    }

    fun setupInitView(){

        val serviceTypeAdapter: ArrayAdapter<String> = ArrayAdapter<String>(this,
            R.layout.spinner_tv, R.id.tv_title, serviceTypeList)

        sp_servicetype.adapter = serviceTypeAdapter
        sp_servicetype.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

                var selectedItem = serviceTypeList[position]

                if(position != 0){

                    var bundle = Bundle()
                    bundle.putInt("type", position)
                    sp_servicetype.setSelection(0)
                    openActivity(activity =  ServiceActivity::class, extras = bundle )

                }

            }

        }

    }
}