package com.bankas.androiddeveloperassessment

import android.app.ProgressDialog
import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity

open class BaseActivity : AppCompatActivity(){

    lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    fun showProgressDialog(){
        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading")
        progressDialog.show()
    }

    fun hideProgressDialog(){
        progressDialog?.dismiss()
    }
}